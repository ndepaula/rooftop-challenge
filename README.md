# rooftop-challenge

The objective of this challenge is to develop a function that solves a riddle. An API will return an array of unordered strings, and you must sort it with the help of an endpoint, which will guide you through the process. In addition, you must add at least one test (unitary or functional) to your project.

## Requirements
Node.js v14+

git clone https://gitlab.com/ndepaula/rooftop-challenge.git

## Installation
```bash
npm i
```
## Proyect Execution
```bash
npm start
```
## Test Execution
```bash
npm run test
```

## Usage
you can reuse the file file.env by renaming it .env
```
## Important
This project has an algorithm that saves API consumption. It is inspired by the gas savings of Smart Contracts. 
When the execution of a block is repeated, it is validated in a local database (json file) and its pre-saved 
solution is displayed.