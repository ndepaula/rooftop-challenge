require("dotenv").config();
const { storeProcedure } = require("./src/util/handleStorage");
const {
  getData,
  obtainToken,
  checkSolution,
} = require("./src/services/endpoints");
const { mail } = require("./src/config/index");
const { backup } = require("./src/util/handleStorage");
const { check } = require("./src/check");

const app = async () => {
  const operationalSavings = await backup(mail);

  if (!operationalSavings) {
    const token = await obtainToken();
    const blocksToSort = await getData(token);

    console.log(blocksToSort);

    const sortedBlocks = await check(blocksToSort, token);

    const finish = await checkSolution(sortedBlocks.join(""), token);

    if (finish) {
      console.log(
        `Se finalizo de manera exitosa el ordenamiento: ${sortedBlocks.join(
          ""
        )}`
      );
      await storeProcedure(mail.tokenEmail, sortedBlocks.join(""));
    } else {
      console.error("Hubo un error en el algoritmo de ordenamiento");
    }
  }
};
app();
