const { read, save, recovery } = require("./storage");

const storeProcedure = async (mail, block) => {
  const isSaved = await read(mail);

  if (!isSaved) {
    save(mail, block);
    console.log(
      "Se almaceno el registro en la base local para ahorro operacional"
    );
  }
};

const backup = async (mail) => {
  const isSaved = await recovery(mail);

  if (isSaved) {
    console.log(
      "Esta operacion fue ejecutada con anterioridad, no se consume la api y se muestra el bloque ordenado en la base de datos"
    );
    console.log(isSaved);
    return isSaved;
  }
};

module.exports = {
  storeProcedure,
  backup,
};
