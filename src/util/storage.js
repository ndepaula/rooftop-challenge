const fs = require("fs");

const save = async (mail, blocks) => {
  const blocksSave = fs.readFileSync("blockDatabase.json", "utf-8");

  let blocksJson = JSON.parse(blocksSave);

  try {
    const blocksDone = {
      key: mail,
      blocks: blocks,
    };
    blocksJson.push(blocksDone);
    const blockObject = JSON.stringify(blocksJson);
    fs.unlinkSync("blockDatabase.json");
    fs.appendFileSync("blockDatabase.json", blockObject, "utf-8");
  } catch (error) {
    console.log(error);
  }
};

const read = async (mail) => {
  try {
    const blocksSave = fs.readFileSync("blockDatabase.json", "utf-8");
    let blocks = JSON.parse(blocksSave);
    const result = blocks.filter((block) => block.key == mail.tokenEmail);
    return result.length;
  } catch (error) {
    console.log(error);
  }
};
const recovery = async (mail) => {
  try {
    const blocksSave = fs.readFileSync("blockDatabase.json", "utf-8");
    let blocks = JSON.parse(blocksSave);
    const result = blocks.filter((block) => block.key == mail.tokenEmail);

    if (result.length !== 0) {
      return result[0].blocks;
    } else {
      return 0;
    }
  } catch (error) {
    console.log(error);
  }
};

module.exports = {
  save,
  read,
  recovery,
};
