const config = {
    api: {
      tokenPath: process.env.PATH_TOKEN,
      blockPath: process.env.PATH_BLOCKS,
      checkPath: process.env.PATH_CHECK,
    },
  
    mail: {
      tokenEmail: process.env.TOKEN_EMAIL,
    },

  };
  
  module.exports = {
    ...config,
  };
  