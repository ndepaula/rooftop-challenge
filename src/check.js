const { checkOrder } = require("./services/endpoints");

const check = async (blocks = [], token) => {
  let index = 0;

  while (blocks.length > 2 && index !== blocks.length - 2) {
    for (let i = index + 1; i < blocks.length; i++) {
      const isSort = await checkOrder(
        [blocks[index], blocks[i]],
        token
      );

      if (isSort) {
        const cut = blocks.splice(i, 1);
        blocks.splice(index + 1, 0, cut[0]);
        index++;
        break;
      }
    }
  }

  return blocks;
};

module.exports = {
  check,
};
