const axios = require("axios");
const {api, mail} = require("../config");

const obtainToken = async () => {
  try {
    const {
      data: { token },
    } = await axios.get(api.tokenPath, {
      params: {
        email:  mail.tokenEmail,
      },
    });
    console.log("Token API")
    return token;
  } catch (error) {
    console.error("Error obtaining token");
  }
};


const getData = async (token) => {
  try {
    const {
      data: { data },
    } = await axios.get(api.blockPath, {
      params: { token },
    });
    console.log("getBlock API")
    return data;
  } catch (error) {
    console.error("Error get blocks data");
  }
};



const checkOrder = async (blocks, token) => {
  try {
    const {
      data: { message },
    } = await axios.post(
      api.checkPath,
      {
        blocks,
      },
      { params: { token } }
    );
    console.log("checkingBlocks API")
    return message;
  } catch (error) {
    console.error("Error checking order the blocks");
  }
};

const checkSolution = async (encoded, token) => {
  try {
    const {
      data: { message },
    } = await axios.post(
      api.checkPath,
      {
        encoded,
      },
      {
        params: { token },
      }
    );
    console.log("checksolution API")
    return message;
  } catch (error) {
    console.error("Error checking the solution");
  }
};

module.exports = {
  obtainToken,
  getData,
  checkOrder,
  checkSolution,
};
