const chai  = require("chai");
const sinon = require("sinon");
const proxyquire = require("proxyquire");
const { fakeBlocks } = require("../test/test-file/fakeBlocks.js");
const { fakeToken } = require("./test-file/fakeBlocks");

describe("Sort Blocks check()", () => {
  it("Happy path", async () => {
    const checkOrderStub = sinon.stub().returns(true);

    const { check } = proxyquire("../src/check", {
      "../src/services/endpoints": {
        checkOrder: checkOrderStub,
      },
    });

    const result = await check(fakeBlocks, fakeToken);
    chai.expect(result).to.eql(fakeBlocks);
  });
});
